module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'react', 'react-hooks', 'lodash'],
  parserOptions: {
    project: 'tsconfig.json',
  },
  extends: ['react-app', 'eslint:recommended', 'plugin:@typescript-eslint/recommended', 'prettier'],
  rules: {
    'no-prototype-builtins': 0,
    'no-console': ['warn', { allow: ['info', 'warn', 'error'] }],

    // https://www.npmjs.com/package/eslint-plugin-react-hooks#advanced-configuration
    'react-hooks/exhaustive-deps': ['warn'],

    // TS-related
    // You can find all the TS rules here:
    // https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin/docs/rules
    '@typescript-eslint/ban-ts-comment': ['error'],
    '@typescript-eslint/no-empty-interface': 0,
    '@typescript-eslint/explicit-module-boundary-types': 0,
    '@typescript-eslint/ban-types': 0,
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/no-non-null-assertion': 0,
    '@typescript-eslint/explicit-function-return-type': 0,
    '@typescript-eslint/camelcase': 0,
    '@typescript-eslint/array-type': ['error', { default: 'array-simple' }],
    '@typescript-eslint/method-signature-style': ['error', 'property'],
    'no-dupe-class-members': 'off',
    '@typescript-eslint/no-dupe-class-members': ['error'],
    '@typescript-eslint/no-extra-non-null-assertion': ['error'],
    '@typescript-eslint/no-for-in-array': ['error'],
    '@typescript-eslint/no-implied-eval': ['error'],
    '@typescript-eslint/prefer-string-starts-ends-with': ['error'],
    '@typescript-eslint/unified-signatures': ['error'],
    'no-loop-func': 'off',
    '@typescript-eslint/no-loop-func': ['error'],
    '@typescript-eslint/no-confusing-non-null-assertion': ['error'],
    '@typescript-eslint/prefer-includes': ['error'],
    '@typescript-eslint/await-thenable': ['error'],
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': ['error'],
    'no-unused-expressions': 'off',
    '@typescript-eslint/no-unused-expressions': ['error', { allowShortCircuit: true, allowTernary: true }],
    // Fixes https://stackoverflow.com/questions/63818415/react-was-used-before-it-was-defined
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],

    'lodash/import-scope': [2, 'method'],

    '@typescript-eslint/restrict-plus-operands': ['error'],
    '@typescript-eslint/prefer-optional-chain': ['error'],
    '@typescript-eslint/prefer-nullish-coalescing': ['error'],
    'default-param-last': 'off',
    '@typescript-eslint/default-param-last': ['error'],
    'no-duplicate-imports': 'off',
    '@typescript-eslint/no-duplicate-imports': ['error'],
  },
  env: {
    browser: true,
    node: true,
  },
  globals: {},
};
