import { database } from './clientApp';
import { Property } from '@libs/models';

export const getRooms = () => {
  const rooms = new Map<string, Property>();
  return database
    .collection('rooms')
    .get()
    .then((roomsSnapshot) => {
      roomsSnapshot.forEach((room) =>
        rooms.set(room.id, room.data() as Property),
      );
      return rooms;
    });
};
