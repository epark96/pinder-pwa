import firebase from 'firebase/app';
import 'firebase/firestore';

const clientCredentials = {
  apiKey: 'AIzaSyAVxgvtymsd93Gb0lk5T5oE4VW_xKiZL2Y',
  authDomain: 'pinder-553fe.firebaseapp.com',
  projectId: 'pinder-553fe',
  storageBucket: 'pinder-553fe.appspot.com',
  messagingSenderId: '537508892629',
  appId: '1:537508892629:web:71ec4f1c2a34be6a7b375c',
  measurementId: 'G-L264V6F4LY',
};

if (!firebase.apps.length) {
  firebase.initializeApp(clientCredentials);
}

export const database = firebase.firestore();
