import { database } from './clientApp';

export const getRoom = (id: string) => {
  return database
    .collection('rooms')
    .doc(id)
    .get()
    .then((roomSnapshot) => {
      return roomSnapshot.data();
    });
};
