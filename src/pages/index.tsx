import React, { useMemo, useState } from 'react';
import { useRouter } from 'next/router';
import { Box } from '@theme-ui/components';
import TinderCard from 'react-tinder-card';

import { Header } from '@libs/components/header';
import { Main } from '@libs/components/main';
import { PropertyCard } from '@libs/components/property-card';

import { getRooms } from '../firebase/getRooms';
import { useOnMount } from '../hooks/useOnMount';
import { SwipeButtons } from '@libs/components/swipe-buttons';
import { Property } from '@libs/models';

type SwipeDirection = 'left' | 'right' | 'up' | 'down';

const IndexPage = () => {
  const router = useRouter();
  const [roomItems, setRoomItems] = useState<Property[]>([]);
  const alreadyRemoved: number[] = [];

  const swiped = (dir: SwipeDirection, propertyId: number) => {
    alreadyRemoved.push(propertyId);
  };

  const outOfFrame = (propertyId: number) => {
    const state = roomItems.filter(
      (property) => property.propertyId !== propertyId,
    );
    setRoomItems(state);
  };

  const swipe = (dir: SwipeDirection) => {
    const propertiesLeft = roomItems.filter(
      (i) => !alreadyRemoved.includes(i.propertyId),
    );
    if (propertiesLeft.length) {
      const toBeRemoved = propertiesLeft[propertiesLeft.length - 1].propertyId;
      const index = roomItems.map((i) => i.propertyId).indexOf(toBeRemoved);
      alreadyRemoved.push(toBeRemoved);
      childRefs[index]?.current?.swipe(dir);
    }
  };

  const onPropertyTap = (propertyId: string) => {
    router.push(`/property/${propertyId}`);
  };

  useOnMount(() => {
    getRooms().then((roomsData) => {
      setRoomItems([...roomsData].map(([id, property]) => property));
    });
  });

  const childRefs = useMemo(
    () =>
      Array(roomItems?.length)
        .fill(0)
        .map((i) => React.createRef()),
    [], // eslint-disable-line
  );

  return (
    <>
      <Header />
      <Main>
        <Box
          sx={{
            position: 'relative',
            justifyContent: 'center',
          }}
        >
          {roomItems?.map((property, index) => (
            <Box sx={{ position: 'absolute' }} key={index}>
              <TinderCard
                ref={childRefs[index] as any}
                onSwipe={(dir: SwipeDirection) =>
                  swiped(dir, property.propertyId)
                }
                onCardLeftScreen={() => outOfFrame(property.propertyId)}
              >
                <PropertyCard
                  propertyId={String(property.propertyId)}
                  property={property}
                  onTap={onPropertyTap}
                />
              </TinderCard>
            </Box>
          ))}
        </Box>
        <Box
          sx={{
            position: 'fixed',
            bottom: 2,
            left: 0,
            height: '10%',
            width: '100%',
          }}
        >
          <SwipeButtons
            onDisLikeClick={() => swipe('left')}
            onLikeClick={() => swipe('right')}
          />
        </Box>
      </Main>
    </>
  );
};

export default IndexPage;

export async function getServerSideProps() {
  return {
    props: {},
  };
}
