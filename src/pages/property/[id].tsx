import React, { useState } from 'react';

import { Header } from '@libs/components/header';
import { Main } from '@libs/components/main';
import { PropertyInfo } from '@libs/components/property-info';
import { useRouter } from 'next/router';
import { useOnMount } from '../../hooks/useOnMount';
import { getRoom } from '../../firebase/getRoom';
import { Property } from '@libs/models';

const PropertyProfilePage = () => {
  const router = useRouter();
  const [property, setProperty] = useState<Property | undefined>(undefined);

  useOnMount(() => {
    const { id } = router.query;
    if (!id || Array.isArray(id)) {
      return;
    }
    getRoom(id).then((room) => {
      setProperty(room as Property);
    });
  });

  return (
    <>
      <Header />
      <Main pb={5}>{property && <PropertyInfo property={property} />}</Main>
    </>
  );
};

export default PropertyProfilePage;

export async function getServerSideProps() {
  return {
    props: {},
  };
}
