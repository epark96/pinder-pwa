import React from 'react';
import { AppProps } from 'next/app';
import Head from 'next/head';

import { ApolloProvider } from '@apollo/client';

import { ThemeProvider as EmotionThemeProvider } from '@emotion/react';
import { ThemeProvider } from '@theme-ui/theme-provider';

import { useApollo } from '@libs/apollo/apolloClient';
import { theme } from '@libs/theme';

export default function App({ Component, pageProps }: AppProps) {
  const apolloClient = useApollo(pageProps);

  return (
    <ThemeProvider theme={theme}>
      <EmotionThemeProvider theme={theme}>
        <ApolloProvider client={apolloClient}>
          <Head>
            <meta charSet="utf-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta
              name="viewport"
              content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
            />
            <meta name="description" content="Description" />
            <meta name="keywords" content="Keywords" />
            <title>Pinder</title>

            <link rel="manifest" href="/manifest.json" />
            <link
              href="/icons/favicon-16x16.png"
              rel="icon"
              type="image/png"
              sizes="16x16"
            />
            <link
              href="/icons/favicon-32x32.png"
              rel="icon"
              type="image/png"
              sizes="32x32"
            />
            <link rel="apple-touch-icon" href="/icons/apple-icon.png"></link>
            <meta name="theme-color" content="#183C67" />
          </Head>

          <Component {...pageProps} />
        </ApolloProvider>
      </EmotionThemeProvider>
    </ThemeProvider>
  );
}
