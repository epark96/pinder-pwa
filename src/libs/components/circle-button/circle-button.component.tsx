import React from 'react';
import { Button, ButtonProps } from '@theme-ui/components';

export const CircleButton: React.FC<ButtonProps> = (props) => {
  return (
    <Button
      variant="secondary"
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '50%',
        width: 55,
        height: 55,
        padding: 0,
        boxShadow: 'light',
      }}
      {...props}
    />
  );
};
