import React from 'react';
import { Box, Heading, Image, Text } from '@theme-ui/components';

import { PropertyFeatures } from '@libs/components/property-features';

import { PropertyInfoProps } from './property-info.props';

export const PropertyInfo: React.FC<PropertyInfoProps> = ({ property }) => {
  const { name, price, amenities, images } = property;
  return (
    <Box
      sx={{
        flexDirection: 'column',
        backgroundColor: 'white',
        height: '100%',
      }}
    >
      <Image
        src={images[0]}
        mb={3}
        sx={{
          display: 'block',
          width: '100%',
          height: '50%',
          objectFit: 'cover',
          borderRadius: 'md',
        }}
      />

      <Heading mb={2} color="primary">
        {name}
      </Heading>

      <Text as="p" mb={1} sx={{ fontWeight: 'body' }}>
        from{' '}
        <Text color="primary" sx={{ fontWeight: 'bold' }}>
          ${price}
        </Text>{' '}
        / week
      </Text>

      <Text as="p" mb={2} sx={{ fontWeight: 'body' }}>
        <Text color="primary" sx={{ fontWeight: 'bold' }}>
          0
        </Text>{' '}
        miles away
      </Text>

      <Text as="h3" mb={1} color="primary">
        Room Features
      </Text>

      <PropertyFeatures features={amenities} />
    </Box>
  );
};
