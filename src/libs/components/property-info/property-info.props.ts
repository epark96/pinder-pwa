import { Property } from '@libs/models';

export interface PropertyInfoProps {
  property: Property;
}
