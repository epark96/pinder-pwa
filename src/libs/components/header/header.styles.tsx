import React from 'react';
import { Flex, BoxProps } from '@theme-ui/components';

export const HeaderRoot: React.FC<BoxProps> = (props) => (
  <Flex
    as="header"
    bg="primary"
    p={2}
    sx={{
      height: 60,
      alignItems: 'center',
      justifyContent: 'space-around',
    }}
    {...props}
  />
);
