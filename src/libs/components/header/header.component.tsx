/** @jsxRuntime classic */
/** @jsx jsx */
import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { jsx } from 'theme-ui';
import { BoxProps, Flex } from '@theme-ui/components';

import { Logo } from '@libs/components/logo';
import { ArrowLeft } from '@libs/icons/arrow-left';

import { HeaderRoot } from './header.styles';

const ActionWrapper: React.FC<BoxProps> = (props) => (
  <Flex sx={{ width: 40, alignItems: 'center' }} {...props} />
);

export const Header: React.FC = () => {
  const router = useRouter();
  const showReturn = router.pathname === '/property/[id]';

  return (
    <HeaderRoot>
      <ActionWrapper>
        {showReturn && (
          <Link href="/">
            <a sx={{ display: 'block', lineHeight: 1 }}>
              <ArrowLeft width={30} height={30} fill="accent" />
            </a>
          </Link>
        )}
      </ActionWrapper>

      <Flex sx={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
        <Logo width={40} height={40} />
      </Flex>

      <ActionWrapper />
    </HeaderRoot>
  );
};
