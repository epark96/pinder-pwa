import React from 'react';

export const Logo: React.FC<{ width: number; height: number }> = (props) => {
  return (
    <svg
      id="prefix__Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      x={0}
      y={0}
      viewBox="0 0 256 256"
      xmlSpace="preserve"
      {...props}
    >
      <style />
      <linearGradient
        id="prefix__SVGID_1_"
        gradientUnits="userSpaceOnUse"
        x1={119.294}
        y1={63.404}
        x2={134.123}
        y2={52.72}
      >
        <stop offset={0} stopColor="#ff5a53" />
        <stop offset={0.172} stopColor="#ff5456" />
        <stop offset={0.408} stopColor="#ff425d" />
        <stop offset={0.681} stopColor="#ff2569" />
        <stop offset={0.96} stopColor="#ff0079" />
      </linearGradient>
      <path
        d="M117.4 67.1c2.7-.6 5.4-.9 8.2-.9 3.1 0 6.1.2 9.2.9-.6-3-.9-6.1-.9-9.2 0-2.6.3-5.3.9-7.9-2.7.6-5.4.9-8.2.9-3.1 0-6.2-.2-9.2-.9 1.2 5.6 1.2 11.4 0 17.1z"
        fill="url(#prefix__SVGID_1_)"
      />
      <linearGradient
        id="prefix__SVGID_2_"
        gradientUnits="userSpaceOnUse"
        x1={117.854}
        y1={206.418}
        x2={132.683}
        y2={195.733}
      >
        <stop offset={0} stopColor="#ff5a53" />
        <stop offset={0.172} stopColor="#ff5456" />
        <stop offset={0.408} stopColor="#ff425d" />
        <stop offset={0.681} stopColor="#ff2569" />
        <stop offset={0.96} stopColor="#ff0079" />
      </linearGradient>
      <path
        d="M133.3 192.9c-2.7.6-5.4.9-8.2.9-3.1 0-6.1-.2-9.2-.9.6 3 .9 6.1.9 9.2 0 2.6-.3 5.3-.9 7.9 2.7-.6 5.4-.9 8.2-.9 3.1 0 6.2.2 9.2.9-1.1-5.6-1.1-11.4 0-17.1z"
        fill="url(#prefix__SVGID_2_)"
      />
      <linearGradient
        id="prefix__SVGID_3_"
        gradientUnits="userSpaceOnUse"
        x1={34.709}
        y1={195.22}
        x2={238.07}
        y2={48.691}
      >
        <stop offset={0} stopColor="#ff5a53" />
        <stop offset={0.172} stopColor="#ff5456" />
        <stop offset={0.408} stopColor="#ff425d" />
        <stop offset={0.681} stopColor="#ff2569" />
        <stop offset={0.96} stopColor="#ff0079" />
      </linearGradient>
      <path
        d="M222.2 0H33.8C15.1 0 0 15.1 0 33.8v188.4C0 240.9 15.1 256 33.8 256h188.4c18.7 0 33.8-15.1 33.8-33.8V33.8C256 15.1 240.9 0 222.2 0zM25.8 131.5l9.8-9.8 8.8 8.8 6-6-8.8-8.8 9.8-9.8 8.8 8.8 38.6-38.6c8.2-10.5 7.9-25.3-.7-35.8l9.6-9.6c5.3 4.3 11.9 6.8 18.8 6.5 6.9-.1 13.5-2.7 18.6-7.2l9.7 9.6c-4.4 5.1-6.9 11.6-7.1 18.4-.1 6.8 2.2 13.4 6.4 18.6l-9.6 9.6c-10.6-8.6-25.4-8.7-36.1-.1l-64 64-18.6-18.6zm189.4 6.8l-8.8-8.8-6 6 8.8 8.8-9.8 9.8-8.8-8.8-38.6 38.6c-8.2 10.5-7.9 25.3.7 35.8l-9.6 9.6c-5.3-4.3-11.9-6.8-18.8-6.5-6.9.1-13.5 2.7-18.6 7.2l-9.7-9.6c4.4-5.1 6.9-11.6 7.1-18.4.1-6.8-2.2-13.4-6.4-18.6l9.6-9.6c10.6 8.6 25.4 8.7 36.1.1l64-64 18.6 18.6-9.8 9.8z"
        fill="url(#prefix__SVGID_3_)"
      />
    </svg>
  );
};
