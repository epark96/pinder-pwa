import React from 'react';
import { BoxProps, Box } from '@theme-ui/components';

export const Main: React.FC<BoxProps> = (props) => {
  const { sx, ...rest } = props;

  return (
    <Box
      as="main"
      bg="background"
      p={3}
      sx={{
        height: 'calc(100vh - 60px)',
        ...sx,
      }}
      {...rest}
    />
  );
};
