import { Property } from '../../models';

export interface PropertyCardProps {
  propertyId: string;
  property: Property;
  onTap: (propertyId: string) => void;
}

export interface SwipeablePropertyCardProps extends PropertyCardProps {}
