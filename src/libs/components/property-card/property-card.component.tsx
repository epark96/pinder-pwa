import React, { useState } from 'react';
import { Box, Card, Flex, Heading, Image, Text } from '@theme-ui/components';
import styled from '@emotion/styled';

import TinderCard from 'react-tinder-card';

import {
  PropertyCardProps,
  SwipeablePropertyCardProps,
} from './property-card.props';

export const PropertyCard: React.FC<PropertyCardProps> = ({
  propertyId,
  property,
  onTap,
}) => {
  const [clickPosition, setClickPosition] = useState({
    clientX: 0,
    clientY: 0,
  });

  /***
   * These two 'silly' functions are needed because a) web and mobile handle
   * clicks with separate events and b) we need to determine if a click is
   * a tap or a hold. In this instance, we need taps.
   * @param ev Event object passed by React event handlers
   */
  const onMouseDown = (ev: React.MouseEvent | React.TouchEvent) => {
    if (ev.nativeEvent instanceof MouseEvent) {
      setClickPosition({
        clientX: ev.nativeEvent.clientX,
        clientY: ev.nativeEvent.clientY,
      });
    } else if (ev.nativeEvent instanceof TouchEvent) {
      setClickPosition({
        clientX: Math.round(ev.nativeEvent.touches[0].clientX),
        clientY: Math.round(ev.nativeEvent.touches[0].clientY),
      });
    }
  };

  /***
   * These two 'silly' functions are needed because a) web and mobile handle
   * clicks with separate events and b) we need to determine if a click is
   * a tap or a hold. In this instance, we need taps.
   * @param ev Event object passed by React event handlers
   */
  const onMouseUp = (ev: React.MouseEvent | React.TouchEvent) => {
    const mouseEv = ev.nativeEvent;
    if (
      mouseEv instanceof MouseEvent &&
      mouseEv.clientX &&
      mouseEv.clientX === clickPosition.clientX &&
      mouseEv.clientY &&
      mouseEv.clientY === clickPosition.clientY
    ) {
      onTap(propertyId);
    } else if (
      mouseEv instanceof TouchEvent &&
      Math.round(mouseEv.changedTouches[0].clientX) === clickPosition.clientX &&
      Math.round(mouseEv.changedTouches[0].clientY) === clickPosition.clientY
    ) {
      onTap(propertyId);
    }
  };

  return (
    <Card
      sx={{
        boxShadow: 'light',
        borderRadius: 'md',
        overflow: 'hidden',
        width: 'calc(100vw - 30px)',
        height: 500,
      }}
      onMouseDown={onMouseDown}
      onMouseUp={onMouseUp}
      onTouchStart={onMouseDown}
      onTouchEnd={onMouseUp}
    >
      <Flex
        sx={{
          alignItems: 'center',
          justifyContent: 'center',
          height: 400,
        }}
      >
        <Image
          src={property.images[0]}
          sx={{
            display: 'block',
            width: '100%',
            height: '100%',
            objectFit: 'cover',
          }}
        />
      </Flex>
      <Box
        p={3}
        sx={{
          height: 100,
        }}
      >
        <Heading as="h2" color="primary" mb={2}>
          {property.name}
        </Heading>

        <Text as="p" mb={1}>
          from{' '}
          <Text color="primary" sx={{ fontWeight: 'bold' }}>
            ${property.price}
          </Text>{' '}
          / week
        </Text>
      </Box>
    </Card>
  );
};

const SwipeablePropertyCardRoot = styled(TinderCard)`
  position: absolute;
`;

export const SwipeablePropertyCard: React.FC<SwipeablePropertyCardProps> = (
  props,
) => {
  return (
    <SwipeablePropertyCardRoot>
      <PropertyCard {...props} />
    </SwipeablePropertyCardRoot>
  );
};
