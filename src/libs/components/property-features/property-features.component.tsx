/** @jsx jsx */
import { jsx } from 'theme-ui';
import React from 'react';
import { Box, Text } from '@theme-ui/components';

import { BulletPoint } from '@libs/icons/bullet-point';

import { PropertyFeaturesProps } from './property-features.props';

export const PropertyFeatures: React.FC<PropertyFeaturesProps> = ({
  features = [],
}) => {
  return (
    <Box>
      <ul
        sx={{
          margin: 0,
          padding: 0,
          listStyle: 'none',
        }}
      >
        {features.map((feature, index) => (
          <li key={index}>
            <BulletPoint fill="accent" height={10} width={10} />
            <Text ml={2}>{feature}</Text>
          </li>
        ))}
      </ul>
    </Box>
  );
};
