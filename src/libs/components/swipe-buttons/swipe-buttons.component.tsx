import React from 'react';
import { Flex } from '@theme-ui/components';
import { CircleButton } from '@libs/components/circle-button';
import { HeartIcon } from '@libs/icons/heart';
import { CrossIcon } from '@libs/icons/cross';
import { SwipeButtonProps } from './swipe-button.props';

const commonIconProps: React.SVGProps<SVGSVGElement> = {
  width: 24,
  height: 24,
};

export const SwipeButtons: React.FC<SwipeButtonProps> = ({
  onLikeClick,
  onDisLikeClick,
}) => {
  return (
    <Flex
      p={3}
      bg="background"
      sx={{
        width: '100%',
        borderRadius: 'sm',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}
    >
      <CircleButton onClick={onDisLikeClick}>
        <CrossIcon {...commonIconProps} />
      </CircleButton>

      <CircleButton onClick={onLikeClick}>
        <HeartIcon {...commonIconProps} />
      </CircleButton>
    </Flex>
  );
};
