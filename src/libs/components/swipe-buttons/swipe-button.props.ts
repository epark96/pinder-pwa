export type SwipeButtonType = 'like' | 'dislike';

export interface SwipeButtonProps {
  onLikeClick: () => void;
  onDisLikeClick: () => void;
}
