export interface Property {
  name: string;
  price: number;
  location: {
    latitude: number;
    longitude: number;
  };
  propertyId: number;
  amenities: string[];
  images: string[];
}
