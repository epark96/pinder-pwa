import React from 'react';
import { theme } from '@libs/theme';
import { getColor } from '@theme-ui/color';

export const BulletPoint = (props: React.SVGProps<SVGSVGElement>) => {
  const fill = getColor(theme, props.fill);

  return (
    <svg
      width={props.width}
      height={props.height}
      viewBox="0 0 100 100"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="50" cy="50" r="50" fill={fill} />
    </svg>
  );
};
