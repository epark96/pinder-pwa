import { Theme } from '@theme-ui/css';

export const theme: Theme = {
  breakpoints: ['40em', '56em', '64em'],

  colors: {
    body: '#fff',
    text: '#000',
    primary: '#183C67',
    accent: '#ecbc3e',
    secondary: '#dd5657',
    success: '#3cba83',
    danger: '#ff5c5c',
    background: '#fff',
    muted: '#d4d4d4',
  },

  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],

  fonts: {
    body:
      '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", sans-serif',
    heading: 'inherit',
  },
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 96],

  fontWeights: {
    body: 400,
    heading: 700,
    bold: 700,
  },

  lineHeights: {
    body: 1.5,
    heading: 1.125,
  },

  radii: {
    sm: 8,
    md: 16,
    lg: 24,
  },

  shadows: {
    light: '0 10px 20px rgba(0, 0, 0, 0.1)',
  },

  buttons: {
    primary: {
      bg: 'primary',
      color: 'background',
    },

    secondary: {
      color: 'primary',
      bg: 'background',
      '&:hover': {
        bg: 'primary',
        color: 'background',
        fill: 'background',
      },
    },
  },

  styles: {
    root: {
      fontFamily: 'body',
      lineHeight: 'body',
      fontWeight: 'body',
    },
    a: {
      textDecoration: 'none',
    },
  },
};
