import { Theme as ThemeUITheme } from '@theme-ui/css';

declare module '@emotion/react' {
  export interface Theme extends ThemeUITheme {}
}
