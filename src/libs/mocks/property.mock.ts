import { Property } from '../models';

export const PROPERTY_MOCK: Property = {
  name: 'Terrakion Mock',
  propertyId: 1,
  price: 125,
  images: [
    'https://www.padsplit.com/img/psproperty/253/img_1587154537.8206844.jpg',
  ],
  amenities: [
    'Shared full bathroom',
    'Full bed',
    'Reach-in closet',
    'Punch Code Lock',
    'Allows up to 1 adult occupant(s)',
  ],
  location: {
    latitude: 0,
    longitude: 0,
  },
};
