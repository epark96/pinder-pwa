/**
 * ```ts
 * Custom useEffect hook, which is invoked only once (on mount)
 * ```
 */

import { useEffect } from 'react';

export function useOnMount(effect: () => void) {
  useEffect(() => {
    effect();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
}
